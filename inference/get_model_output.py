from PIL import Image
import numpy as np
import requests
import argparse
import io
import base64

# Specify the arguments for the script
parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output", help="Output file path", required=True)
args = parser.parse_args()

# Specify the URL of the endpoint
url = "http://localhost:8003/generate"

# Send a GET request to the endpoint
response = requests.get(url)

if response.status_code == 200:
    # Get the base64 encoded image from the response
    encoded_image = response.json()['image']

    # Decode the base64 image
    decoded_image = base64.b64decode(encoded_image)

    # Convert the decoded image to a PIL image
    image = Image.open(io.BytesIO(decoded_image))

    # Save the image to the specified file path
    image.save(args.output)
else:
    print('Request failed with status code', response.status_code)
    print('Response:', response.text)
