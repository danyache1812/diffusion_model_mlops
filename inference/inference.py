import mlflow
import os
import numpy as np
from dotenv import load_dotenv
from fastapi import FastAPI
import torch
from PIL import Image
import io
import base64
from fastapi.responses import JSONResponse
import sys
sys.path.insert(0, '../src')
from diffusion_model import DiffModel


# Load env variables
load_dotenv()

# Initialize the FastAPI application
app = FastAPI()

# Create class to store deployed model


class Model:
    def __init__(self, model_name, model_stage):
        # Load model from Registry
        self.model = mlflow.pytorch.load_model(
            f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        with torch.no_grad():
            predictions = self.model(data)
        return predictions


# Create model
model_name = os.getenv("MODEL_NAME")
model_stage = os.getenv("MODEL_STAGE")
model = Model(model_name, model_stage)


@app.get("/generate")
async def generate_image():
    # Use the model to generate an array of images
    sampled_images = model.model.sample_images(8)
    sampled_images = model.model.transform_sampled(sampled_images)

    sampled_images = sampled_images.reshape(8, 32, 32, 3)
    grid = np.vstack([np.hstack(sampled_images[i * 4:i * 4 + 4]) for i in range(2)])

    # Convert grid to PIL image
    output_image = Image.fromarray(grid)

    byte_arr = io.BytesIO()
    output_image.save(byte_arr, format='PNG')

    # Convert to base64
    encoded_image = base64.b64encode(byte_arr.getvalue()).decode()

    return JSONResponse(content={'image': encoded_image})

if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(1)
