# Diffusion Model for MLOps course

Authors: Daniil Chesakov, Evgeny Kovalev, Artem Petrov

# Installation
Run these commands to get enviroment ready:

    conda create -n diff_model_env python=3.10
    pip install -r requirements.txt

# Get started

First of all you need to start all the services. To do so run commands:

    cd docker
    cp env.dist .env
    cp serving/env.dist serving/.env

And update those two .env files with proper S3 bucket name.
Also set all necessary configuration in .aws/credential (https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html)

To run all microservices:
    
    docker-compose up -d --build

To stop all microservices:

    docker-compose down

pgadmin:
    
    http://localhost:5050 (see docker-compose.yml)

minio:
    
    http://localhost:9000

mlflow:
    
    http://localhost:5000

Next command to run portainer (analogue Docker Desktop):

   1. docker volume create portainer_data
   2. docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest
   3. https://localhost:9443

# Linters

To install pre-commit hooks, execute `pre-commit install`.

To run pre-commit hooks, execute `pre-commit run --all-files`.

# DVC

In order to use dvc for a first time, you need to:

1. Rename `.dvc/config.dist` into `.dvc/config`
2. Write private s3 bucket name, `access_key_id` and `secret_access_key` to `.dvc/config`.

Then `dvc push` and `dvc pull` will work with the private S3.

Also you could run `dvc repro` to check the pipeline. 
 
# Train, test and save

To test training of the model run:
`python src/train.py  --experiment-name "torch_train_test" --epochs 1`

To train it from scratch:
`python src/train.py  --experiment-name "torch_train_scratch" --epochs 150 ---no-fast-stop`

To fine tune:
`python src/train.py  --experiment-name "torch_train_finetune" --epochs 10 --saved_model {RUN_ID} --no-fast-stop`


Double check that s3 bucket is existing!

To save model from some local weights:
`python src/save_trained_model.py --experiment "{EXPERIMENT_NAME}" --model_path models/{MODEL_NAME}.pth --model_name "{MODEL_NAME}"`
Pretrained weights could be found [here](https://drive.google.com/file/d/1ULh3qWEQYQ7oYCYWLae5zkDIr2z7h4fN/view?usp=sharing).


To generate example of an output of model:
`python inference/get_model_output.py -o example.png`



