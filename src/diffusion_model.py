import torch
from torch.nn import functional as F
import matplotlib.pyplot as plt
import numpy as np

from unet import ResidualUnet


def linear_schedule(beta_0=0.0001, beta_T=0.02, T=1000):
    beta = torch.linspace(beta_0, beta_T, T)
    alpha = 1 - beta
    alpha_hat = torch.cumprod(alpha, dim=0)

    return alpha, beta, alpha_hat


def cos_f(t, T, s):
    return (torch.cos(torch.tensor(((t/T + s)/(1 + s))*torch.pi/2))) ** 2


def cosine_schedule(s=0.008, T=1000):
    alpha_hat = torch.tensor(
        [1.] + [cos_f(t, T, s)/cos_f(0., T, s) for t in range(T+1)])
    alpha = alpha_hat[1:]/alpha_hat[:-1]
    beta = torch.clip(1 - alpha, max=0.999)

    return alpha, beta, alpha_hat[1:]


def get_noise_schedule(schedule_type='linear', **kwargs):
    if schedule_type == 'linear':
        return linear_schedule(**kwargs)
    elif schedule_type == 'cosine':
        return cosine_schedule(**kwargs)
    else:
        raise ValueError("Choose 'linear' or 'cosine' type")


class DiffModel(torch.nn.Module):
    def __init__(self, device='cuda:0', num_classes=None, image_size=32, schedule_type='linear', T=1000):
        super().__init__()
        self.unet_model = ResidualUnet(num_classes=num_classes, device=device)
        self.T = T
        self.image_size = image_size
        self.device = device
        self.num_classes = num_classes

        self.alpha, self.beta, self.alpha_hat = get_noise_schedule(
            schedule_type=schedule_type)
        self.alpha, self.beta, self.alpha_hat = self.alpha.to(
            device), self.beta.to(device), self.alpha_hat.to(device)

    def noise_images(self, images, t):
        alpha_hat_t = self.alpha_hat[t][:, None, None, None]
        eps = torch.randn_like(images)
        noised_images = torch.sqrt(alpha_hat_t) * \
            images + torch.sqrt(1 - alpha_hat_t)*eps
        return noised_images, eps

    def denoise_step(self, noised_images_t, t, img_classes):
        if t > 1:
            noise = torch.randn_like(noised_images_t)
        else:
            noise = torch.zeros_like(noised_images_t)

        t = (torch.ones(noised_images_t.shape[0]) * t).long().to(self.device)
        predicted_noise = self.unet_model(noised_images_t, t, img_classes)

        mu = 1 / torch.sqrt(self.alpha[t][:, None, None, None]) * \
            (noised_images_t - (self.beta[t][:, None, None, None] / torch.sqrt(
                1 - self.alpha_hat[t][:, None, None, None])) * predicted_noise)

        return mu + torch.sqrt(self.beta[t][:, None, None, None]) * noise

    def sample_images(self, n_images):
        self.eval()

        with torch.no_grad():
            x = torch.randn((n_images, 3, self.image_size,
                            self.image_size)).to(self.device)

            for t in reversed(range(1, self.T)):
                if self.num_classes is None:
                    img_classes = None
                else:
                    img_classes = torch.randint(
                        low=0, high=10, size=(n_images, 1), device=self.device)

                x = self.denoise_step(x, t, img_classes)

        x = x.clamp(-1, 1)
        x = (x + 1) / 2
        x = (x * 255).type(torch.uint8)

        self.train()
        return x

    def sample_t(self, n, device):
        return torch.randint(low=1, high=self.T, size=(n,), device=device)

    def forward(self, x, t, y=None):
        pred_noise = self.unet_model(x, t, y)
        return pred_noise

    def train_step(self, train_batch):
        images, _ = train_batch
        images = images.to(self.device)

        t = self.sample_t(images.shape[0], device=images.device)

        noised_images, noise = self.noise_images(images, t)

        pred_noise = self.forward(noised_images, t)

        loss = F.mse_loss(pred_noise, noise)

        return loss

    def transform_sampled(self, sampled_images):
        imgs_batch = sampled_images.cpu().detach().clone()
        imgs_batch = imgs_batch.numpy()
        imgs_batch = np.transpose(imgs_batch, (0, 2, 3, 1))
        return imgs_batch

    def plot_sample(self):
        sampled_images = self.sample_images(8)
        sampled_images = self.transform_sampled(sampled_images)

        # Changed to 2 rows and 4 columns
        _, axs = plt.subplots(2, 4, figsize=(20, 10))
        for i, img in enumerate(sampled_images):
            # Use i // 4 for row and i % 4 for column
            axs[i // 4, i % 4].imshow(img)
            axs[i // 4, i % 4].axis('off')
        plt.show()
