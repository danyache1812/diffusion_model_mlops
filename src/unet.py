import torch
import torch.nn as nn


class UnetConvBlock(nn.Module):
    def __init__(self, in_c, out_c):
        super().__init__()

        self.conv0 = nn.Conv2d(in_c, out_c, kernel_size=3, padding=1)
        self.bn0 = nn.BatchNorm2d(out_c)

        self.conv1 = nn.Conv2d(out_c, out_c, kernel_size=3, padding=1)
        self.bn1 = nn.BatchNorm2d(out_c)

        self.conv2 = nn.Conv2d(out_c, out_c, kernel_size=3, padding=1)
        self.bn2 = nn.BatchNorm2d(out_c)

        self.relu = nn.ReLU()

    def conv(self, inputs, conv, bn, relu, residual=False):
        x = conv(inputs)
        x = bn(x)
        x = relu(x)
        if residual:
            x = x + inputs
        return x

    def forward(self, inputs):
        x0 = self.conv(inputs, self.conv0, self.bn0, self.relu, False)

        x1 = self.conv(x0, self.conv1, self.bn1, self.relu, True)

        x2 = self.conv(x1, self.conv2, self.bn2, self.relu, True)

        return x2


class EncoderBlock(nn.Module):
    def __init__(self, in_c, out_c, emb_dim=32):
        super().__init__()

        self.conv = UnetConvBlock(in_c, out_c)
        self.pool = nn.MaxPool2d((2, 2))

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(
                emb_dim,
                out_c
            ),
        )

    def forward(self, inputs, t):
        x = self.conv(inputs)
        p = self.pool(x)
        emb = self.emb_layer(t)[:, :, None, None].repeat(
            1, 1, p.shape[-2], p.shape[-1])

        return x, p + emb


class DecoderBlock(nn.Module):
    def __init__(self, in_c, out_c, emb_dim=32):
        super().__init__()

        self.up = nn.ConvTranspose2d(
            in_c, out_c, kernel_size=2, stride=2, padding=0)
        self.conv = UnetConvBlock(out_c + out_c, out_c)

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(
                emb_dim,
                out_c
            ),
        )

    def forward(self, inputs, skip, t):
        x = self.up(inputs)
        x = torch.cat([x, skip], axis=1)  # type: ignore
        x = self.conv(x)
        emb = self.emb_layer(t)[:, :, None, None].repeat(
            1, 1, x.shape[-2], x.shape[-1])

        return x + emb


class ResidualUnet(nn.Module):
    def __init__(self, out_size=3, label_emb=None, time_dim=32, num_classes=None, device='cuda:0'):
        super().__init__()
        self.time_dim = time_dim
        self.label_emb = label_emb
        self.device = device
        self.num_classes = num_classes

        # Encoder
        self.e1 = EncoderBlock(3, 64)
        self.e2 = EncoderBlock(64, 128)
        self.e3 = EncoderBlock(128, 256)
        self.e4 = EncoderBlock(256, 512)

        # Bottleneck
        self.b = UnetConvBlock(512, 1024)

        # Decoder
        self.d1 = DecoderBlock(1024, 512)
        self.d2 = DecoderBlock(512, 256)
        self.d3 = DecoderBlock(256, 128)
        self.d4 = DecoderBlock(128, 64)

        # Classifier
        self.outputs = nn.Conv2d(64, out_size, kernel_size=1, padding=0)

        if num_classes is not None:
            self.label_emb = nn.Embedding(num_classes, self.time_dim)

    def pos_encoding(self, t, channels):
        inv_freq = 1.0 / (
            10000
            ** (torch.arange(0, channels, 2, device=self.device).float() / channels)
        )
        inv_freq_expanded = inv_freq.view(1, -1)
        t_expanded = t.view(-1, 1)

        pos_enc_a = torch.sin(t_expanded * inv_freq_expanded)
        pos_enc_b = torch.cos(t_expanded * inv_freq_expanded)
        pos_enc = torch.cat([pos_enc_a, pos_enc_b], dim=-1)
        return pos_enc

    def forward(self, inputs, t, y=None):
        t = t.unsqueeze(-1).type(torch.float)
        t = self.pos_encoding(t, self.time_dim)

        # Encoder
        s1, p1 = self.e1(inputs, t)
        s2, p2 = self.e2(p1, t)
        s3, p3 = self.e3(p2, t)
        s4, p4 = self.e4(p3, t)

        # Bottleneck
        b = self.b(p4)

        # Decoder
        d1 = self.d1(b, s4, t)
        d2 = self.d2(d1, s3, t)
        d3 = self.d3(d2, s2, t)
        d4 = self.d4(d3, s1, t)

        # Classifier
        outputs = self.outputs(d4)

        return outputs
