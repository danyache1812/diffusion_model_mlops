import argparse
import mlflow.pytorch
import torch
import os
from mlflow.tracking import MlflowClient

from diffusion_model import DiffModel

# It should be in ~.aws/credentials, but for some reason it does not work without an explicit setup in the py file
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://localhost:9000"
os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--experiment', '-e', required=True, help='Name of the experiment')
parser.add_argument('--model_path', required=True, help='Path to the model file')
parser.add_argument('--model_name', required=True, help='Model name for logging in MLFlow')
args = parser.parse_args()

mlflow.set_tracking_uri("http://127.0.0.1:8855")
mlflow.set_experiment(args.experiment)
mlflow.pytorch.autolog()

model = DiffModel(device='cuda' if torch.cuda.is_available() else 'cpu', num_classes=None).to('cpu')
print("Loading model")
model.load_state_dict(torch.load(args.model_path, map_location=torch.device('cpu')))
print("Model loaded")

# Start a new MLFlow run and log the model
with mlflow.start_run() as run:
    mlflow.pytorch.log_model(model, args.model_name)

# Print the run id
print("Logged model with run id: {}".format(run.info.run_id))

# Register the model
client = MlflowClient()
model_uri = f"runs:/{run.info.run_id}/{args.model_name}"
client.create_registered_model(args.model_name)
client.create_model_version(args.model_name, model_uri, run.info.run_id)

# Transition the model version to Production stage
latest_version = client.get_latest_versions(args.model_name, stages=["None"])[0]
client.transition_model_version_stage(
    name=args.model_name,
    version=latest_version.version,
    stage="Production",
)
print(f"Model registered as {args.model_name} with version {latest_version.version}")
