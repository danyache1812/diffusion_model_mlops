import logging
import typer
import numpy as np
import os

from diffusion_model import DiffModel
from utils import load_data, save_model, create_dataloader
import torch
from torch import optim
import mlflow
from typing import Optional

logging.basicConfig(level=logging.INFO)

# It should be in ~.aws/credentials, but for some reason it does not work without an explicit setup in the py file
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://localhost:9000"
os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"


def train(
    dataset_path: str = typer.Option("data/"),
    model_path: str = typer.Option("models/model.pth"),
    experiment_name: str = typer.Option("torch_experiment"),
    lr: float = typer.Option(3e-4),
    epochs: int = typer.Option(50),
    batch_size: int = typer.Option(32),
    saved_model: Optional[str] = typer.Option(None),
    fast_stop: bool = typer.Option(True, "--no-fast-stop")
) -> DiffModel:

    mlflow.set_tracking_uri("http://127.0.0.1:8855")
    mlflow.set_experiment(experiment_name)
    mlflow.pytorch.autolog()

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    with mlflow.start_run():

        if saved_model is not None:
            print("Loading pretrained model")
            model = mlflow.pytorch.load_model(f"runs:/{saved_model}")
            model = model.to(device)
            model.device = 'cuda' if torch.cuda.is_available() else 'cpu'
            model.alpha, model.beta, model.alpha_hat = model.alpha.to(
                device), model.beta.to(device), model.alpha_hat.to(device)
            print("Loaded model")
        else:
            print("Initialising model from scratch")
            model = DiffModel(device='cuda' if torch.cuda.is_available(
            ) else 'cpu', num_classes=None).to(device)

        trainset = load_data(dataset_path=dataset_path, dataset='mnist')
        trainloader = create_dataloader(
            trainset, batch_size=batch_size, shuffle=True)

        optimizer = optim.AdamW(model.parameters(), lr=lr)

        logging.info("Train started")
        for epoch in range(epochs):
            batch_losses = []
            for idx, batch in enumerate(trainloader):
                optimizer.zero_grad()
                loss = model.train_step(batch)
                loss.backward()
                optimizer.step()

                batch_losses.append(loss.cpu().detach().item())

                if fast_stop and (idx == 20):
                    break

            epoch_loss = np.mean(batch_losses)

            logging.info(f"{epoch + 1} loss: {epoch_loss:.3f}")
            mlflow.log_metric(key="epoch_loss", value=epoch_loss,
                              step=epoch)  # type: ignore

            if epoch % 20 == 0:
                mlflow.pytorch.log_model(model, f"model_{epoch}eph")

        logging.info("Finished Training")
        mlflow.pytorch.log_model(model, "model_final")
        save_model(model, model_path)

    return model


if __name__ == "__main__":
    typer.run(train)
