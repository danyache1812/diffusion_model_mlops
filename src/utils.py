import numpy as np
import logging
import typer
from pathlib import Path

import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import DataLoader

logging.basicConfig(level=logging.INFO)


def input_transform(image, transform):
    image = np.array(image)
    x_tr = transform(image=image)
    return x_tr['image']


def load_data(
    dataset_path: str = typer.Option("data/"),
    dataset: str = 'mnist'
):
    download = dataset not in dataset_path
    if not download:
        logging.info("Data already exists, skipping download")

    transform = transforms.Compose([
        transforms.Resize((32, 32)),  # Resize the image to 32x32 pixels
        # Convert image to 3 channel
        transforms.Grayscale(num_output_channels=3),
        # Randomly rotate the image within a range of ±10 degrees
        transforms.RandomRotation(10),
        # Randomly change the brightness, contrast, and saturation of an image
        transforms.ColorJitter(
            brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1),
        transforms.ToTensor(),  # Convert PIL Image to tensor
        # Normalize the image with mean and standard deviation
        transforms.Normalize((0.5,), (0.5,))
    ])

    if dataset == 'mnist':
        transform = transforms.Compose([
            transforms.Resize((32, 32)),
            transforms.Grayscale(num_output_channels=3),
            transforms.RandomRotation(10),
            transforms.ColorJitter(
                brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1),
            transforms.ToTensor(),
            transforms.Normalize((0.5,), (0.5,))
        ])
        data = torchvision.datasets.MNIST(
            dataset_path, train=True, download=True, transform=transform)
    elif dataset == 'cifar-10':
        transform = transforms.Compose([
            transforms.Resize((32, 32)),
            transforms.RandomRotation(10),
            transforms.ColorJitter(
                brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1),
            transforms.ToTensor(),
            transforms.Normalize((0.5,), (0.5,))
        ])
        data = torchvision.datasets.CIFAR10(
            dataset_path, download=True, transform=transform)
    else:
        raise ValueError("Choose either mnist or cifar-10 for training")

    return data


def create_dataloader(dataset, batch_size=32, shuffle=True):
    return DataLoader(dataset, batch_size=batch_size, shuffle=shuffle)


def save_model(model: nn.Module, model_path: str) -> None:
    Path(model_path).parents[0].mkdir(parents=True, exist_ok=True)
    torch.save(model.state_dict(), model_path)
    logging.info(f"Saved model {model.__class__.__name__} into {model_path}")


if __name__ == "__main__":
    typer.run(load_data)
